"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const path = require("path");
const _ = require("lodash");
const proofs = {
    prefix: fs_1.readFileSync(path.join(__dirname, 'hw5proofs', 'prefix'), 'utf8').split('\n'),
    step: fs_1.readFileSync(path.join(__dirname, 'hw5proofs', 'step'), 'utf8').split('\n'),
    end: fs_1.readFileSync(path.join(__dirname, 'hw5proofs', 'end'), 'utf8').split('\n'),
};
const fout = fs_1.createWriteStream(path.join(__dirname, 'input', 'hw5.out'), {
    flags: 'w'
});
let [a, b] = _(fs_1.readFileSync(path.join(__dirname, 'input', 'hw5.in'), 'utf8'))
    .split(',')
    .map((line) => { return parseInt(line); })
    .value();
console.log(a, b);
const makeProof = (a, b) => {
    let summ = a + b;
    let left = `0${_.times(a, _.constant('\'')).join('')}`;
    let right = `0${_.times(b, _.constant('\'')).join('')}`;
    let result = `0${_.times(summ, _.constant('\'')).join('')}`;
    fout.write(`|-${left}+${right}=${result}\n`);
    fout.write(proofs.prefix.join('\n'));
    fout.write('\n');
    const makeSteps = (i, sub1, sub2) => {
        if (i >= b)
            return [sub1, sub2];
        fout.write(proofs.step.map((line) => line.replace(/sub1/g, sub1).replace(/sub2/g, sub2))
            .join('\n'));
        fout.write('\n');
        return makeSteps(i + 1, sub1 + '\'', sub2 + '\'');
    };
    let [sub1, sub2] = makeSteps(0, '0', 'a');
    fout.write(proofs.end.map(line => line
        .replace(/sub1/g, sub1)
        .replace(/sub2/g, sub2)
        .replace(/result/g, result)
        .replace(/left/g, left)).join('\n'));
};
makeProof(a, b);
//# sourceMappingURL=hw5.js.map