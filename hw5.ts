import {createWriteStream, readFileSync} from "fs";
import * as path from "path";
import * as _ from "lodash";


const proofs = {
    prefix: readFileSync(path.join(__dirname, 'hw5proofs', 'prefix'), 'utf8').split('\n'),
    step: readFileSync(path.join(__dirname, 'hw5proofs', 'step'), 'utf8').split('\n'),
    end: readFileSync(path.join(__dirname, 'hw5proofs', 'end'), 'utf8').split('\n'),
};

const fout = createWriteStream(path.join(__dirname, 'input', 'hw5.out'), {
    flags: 'w'
});

let [a,b] = _(readFileSync(path.join(__dirname, 'input', 'hw5.in'), 'utf8'))
    .split(',')
    .map((line) => { return parseInt(line); })
    .value();

console.log(a,b);

const makeProof = (a,b) => {
    let summ = a+b;
    let left    = `0${_.times(a, _.constant('\'')).join('')}`;
    let right   = `0${_.times(b, _.constant('\'')).join('')}`;
    let result  = `0${_.times(summ, _.constant('\'')).join('')}`;

    fout.write(   `|-${left}+${right}=${result}\n`);
    fout.write(proofs.prefix.join('\n'));
    fout.write('\n');

    const makeSteps = (i, sub1, sub2) => {
        if (i >= b) return [sub1, sub2];

        fout.write(proofs.step.map((line) => line.replace(/sub1/g, sub1).replace(/sub2/g, sub2))
        .join('\n'));
        fout.write('\n');

        return makeSteps(i+1, sub1+'\'', sub2+'\'');
    };

    let [sub1, sub2] = makeSteps(0, '0', 'a');

    fout.write(
        proofs.end.map(
            line =>
                line
                    .replace(/sub1/g, sub1)
                    .replace(/sub2/g, sub2)
                    .replace(/result/g, result)
                    .replace(/left/g, left)
        ).join('\n')
    )
};

makeProof(a, b);
