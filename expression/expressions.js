"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("./utils");
const _ = require("lodash");
class Expression {
    constructor() {
        this.hash = 0;
    }
    valueOf() {
        return this.hash;
    }
    updateHash() {
        this.hash = 0;
    }
    eval(dict) {
        return false;
    }
}
exports.Expression = Expression;
class Unary extends Expression {
    constructor(value) {
        super();
        this.val = value;
    }
    updateHash() {
        this.hash = utils_1.hashString(this.toString());
    }
}
exports.Unary = Unary;
class Var extends Unary {
    constructor(value) {
        super(value);
        this.updateHash();
    }
    updateHash() {
        this.hash = utils_1.hashString(this.toString());
    }
    toString() {
        return this.val.toString();
    }
    eval(dict) {
        return dict[this.val];
    }
}
exports.Var = Var;
class Any extends Unary {
    constructor(variable, value) {
        super(value);
        this.name = "@";
        this.var = variable;
        this.updateHash();
    }
    toString() {
        return `(@${this.var}${this.val.toString()})`;
    }
}
exports.Any = Any;
class Exists extends Unary {
    constructor(variable, value) {
        super(value);
        this.name = "?";
        this.var = variable;
        this.updateHash();
    }
    toString() {
        return `(?${this.var}${this.val.toString()})`;
    }
}
exports.Exists = Exists;
class Predicate extends Unary {
    constructor(name, values) {
        super(values);
        this.name = name;
        this.updateHash();
    }
    toString() {
        if (_.size(this.val) == 0) {
            return this.name;
        }
        return this.name + "(" + this.val.map(val => val.toString()).join(',') + ")";
    }
}
exports.Predicate = Predicate;
class Next extends Unary {
    constructor(value) {
        super(value);
        this.name = "'";
        this.updateHash();
    }
    toString() {
        return this.val.toString() + this.name;
    }
}
exports.Next = Next;
class Not extends Unary {
    constructor(value) {
        super(value);
        this.name = '!';
        this.updateHash();
    }
    toString() {
        return `(!${this.val.toString()})`;
    }
    eval(dict) {
        return !this.val.eval(dict);
    }
}
exports.Not = Not;
class Binary extends Expression {
    constructor(left, right) {
        super();
        this.left = left;
        this.right = right;
    }
    valueOf() {
        if (!this.hash) {
            this.updateHash();
        }
        return this.hash;
    }
    updateHash() {
        this.hash = utils_1.hashString(this.toString());
    }
    toString() {
        return `(${this.left.toString()}${this.name}${this.right.toString()})`;
    }
    calc(left, right) {
        return false;
    }
    eval(dict) {
        return this.calc(this.left.eval(dict), this.right.eval(dict));
    }
}
exports.Binary = Binary;
class Implication extends Binary {
    constructor(left, right) {
        super(left, right);
        this.name = "->";
        this.updateHash();
    }
    calc(left, right) {
        return (!left) || right;
    }
}
exports.Implication = Implication;
class Conjuction extends Binary {
    constructor(left, right) {
        super(left, right);
        this.name = "&";
        this.updateHash();
    }
    calc(left, right) {
        return left && right;
    }
}
exports.Conjuction = Conjuction;
class Disjuction extends Binary {
    constructor(left, right) {
        super(left, right);
        this.name = "|";
        this.updateHash();
    }
    calc(left, right) {
        return left || right;
    }
}
exports.Disjuction = Disjuction;
class Equals extends Binary {
    constructor(left, right) {
        super(left, right);
        this.name = "=";
    }
}
exports.Equals = Equals;
class Sum extends Binary {
    constructor(left, right) {
        super(left, right);
        this.name = "+";
    }
}
exports.Sum = Sum;
class Mul extends Binary {
    constructor(left, right) {
        super(left, right);
        this.name = "*";
    }
}
exports.Mul = Mul;
//# sourceMappingURL=expressions.js.map