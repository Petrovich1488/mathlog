"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const formalParser_1 = require("./formalParser");
const expressions_1 = require("./expressions");
const _ = require("lodash");
const utils_1 = require("./utils");
const formalAxiomsRaw = [
    "a=b->a'=b'",
    "a=b->a=c->b=c",
    "a'=b'->a=b",
    "!(a'=0)",
    "a+b'=(a+b)'",
    "a+0=a",
    "a*0=0",
    "a*b'=a*b+a"
];
let formalParser = new formalParser_1.FormalParser();
exports.formalAxioms = _.map(formalAxiomsRaw, (rawAxiom) => formalParser.parseExpr(rawAxiom));
function freeSubtract(template, exp, variable, locked = {}, dict = {}) {
    if (template.constructor.name === 'Var') {
        if (template.valueOf() !== variable.valueOf() || locked[template.val.valueOf()]) {
            return template.valueOf() === exp.valueOf();
        }
        else {
            let temp = new Set();
            utils_1.exportFreeVariables(exp, {}, temp);
            if (_.size(_.intersection(Array.from(temp), Object.keys(locked))) !== 0) {
                return false;
            }
            dict[template.valueOf()] = exp;
            return true;
        }
    }
    else if (template.constructor.name === exp.constructor.name) {
        if (template.constructor.name === 'Any' || template.constructor.name === 'Exists') {
            let temptemplate = template;
            locked[temptemplate.var.val.valueOf()] = (locked[temptemplate.var.val.valueOf()] || 0) + 1;
            let result = freeSubtract(temptemplate.val, exp.val, variable, locked, dict);
            locked[temptemplate.var.val.valueOf()]--;
            if (locked[temptemplate.var.val.valueOf()] === 0) {
                delete locked[temptemplate.var.val.valueOf()];
            }
            return result;
        }
        else if (template.constructor.name === 'Predicate') {
            let temptemplate = template;
            let tempexp = exp;
            if (_.size(temptemplate.val) !== _.size(tempexp.val)) {
                return false;
            }
            for (let i = 0; i < _.size(temptemplate.val); i++) {
                if (!freeSubtract(temptemplate.val[i], tempexp.val[i], variable, locked, dict))
                    return false;
            }
            return true;
        }
        else if (template instanceof expressions_1.Unary) {
            return freeSubtract(template.val, exp.val, variable, locked, dict);
        }
        else {
            let temptemplate = template;
            let tempexp = exp;
            if (!freeSubtract(temptemplate.left, tempexp.left, variable, locked, dict)) {
                return false;
            }
            else
                return freeSubtract(temptemplate.right, tempexp.right, variable, locked, dict);
        }
    }
    else
        return false;
}
exports.freeSubtract = freeSubtract;
function isAxiomAny(expr) {
    if (!(expr instanceof expressions_1.Implication) || !(expr.left instanceof expressions_1.Any)) {
        return false;
    }
    let aexpr = expr;
    let aleft = aexpr.left;
    return freeSubtract(aleft.val, aexpr.right, aleft.var, {}, {});
}
exports.isAxiomAny = isAxiomAny;
function isAxiomExists(expr) {
    if (!(expr instanceof expressions_1.Implication) || !(expr.right instanceof expressions_1.Exists)) {
        return false;
    }
    let aexpr = expr;
    let aright = aexpr.right;
    return freeSubtract(aright.val, aexpr.left, aright.var, {}, {});
}
exports.isAxiomExists = isAxiomExists;
function isFormalAxiom(axiom, expression, dict = {}, locked = new Set()) {
    if (axiom.constructor.name === expressions_1.Var.name) {
        if (locked.has(axiom)) {
            return axiom.valueOf() === expression.valueOf();
        }
        else if (!_.isNil(dict[axiom.valueOf()])) {
            return dict[axiom.valueOf()].valueOf() === expression.valueOf();
        }
        else {
            dict[axiom.valueOf()] = expression;
            return true;
        }
    }
    else if (expression.constructor.name === axiom.constructor.name) {
        if (axiom instanceof expressions_1.Any || axiom instanceof expressions_1.Exists) {
            locked.add(axiom.var);
            let result = isFormalAxiom(axiom.val, expression.val, dict, locked);
            locked.delete(axiom.var);
            return result;
        }
        else if (axiom instanceof expressions_1.Predicate) {
            return _.size(axiom.val) === _.size(expression.val) && axiom.val.every((val, index) => isFormalAxiom(axiom.val[index], expression.val[index], dict, locked));
        }
        else if (axiom instanceof expressions_1.Unary) {
            return isFormalAxiom(expression.val, axiom.val, dict);
        }
        else {
            return isFormalAxiom(expression.left, axiom.left, dict) &&
                isFormalAxiom(expression.right, axiom.right, dict);
        }
    }
    else {
        return false;
    }
}
exports.isFormalAxiom = isFormalAxiom;
function isAnyFormalAxiom(expr) {
    return exports.formalAxioms.some((axiom) => isFormalAxiom(axiom, expr)) ||
        (isAxiomAny(expr) || isAxiomExists(expr));
}
exports.isAnyFormalAxiom = isAnyFormalAxiom;
//# sourceMappingURL=formalAxioms.js.map