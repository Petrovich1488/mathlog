import {Parser} from "./parser";
import * as _ from "lodash";
import {Binary, Expression, Unary, Var} from "./expressions";

const axioms = [
    "A -> (B -> A)",
    "(A -> B) -> (A -> B -> C) -> (A -> C)",
    "A -> B -> A & B",
    "A & B -> A",
    "A & B -> B",
    "A -> A | B",
    "A -> B | A",
    "(A -> B) -> (C -> B) -> (A | C -> B)",
    "(A -> B) -> (A -> !B) -> !A",
    "!!A -> A"
];

export const parsedAxioms:Array<Expression> = _.map(axioms, (rawAxiom) => Parser.parseExp(rawAxiom));

export function isAxiom(expression: Expression, axiom: Expression, dict = {}) {
    if (axiom.constructor.name === Var.name) {
        if (!_.isNil(dict[axiom.valueOf()])) {
            return dict[axiom.valueOf()].valueOf() === expression.valueOf();
        } else {
            dict[axiom.valueOf()] = expression;
            return true;
        }
    } else if (expression.constructor.name === axiom.constructor.name) {
        if (axiom instanceof Unary) {
            return isAxiom((expression as Unary).val, axiom.val, dict);
        } else {
            return  isAxiom((expression as Binary).left, (axiom as Binary).left, dict) &&
                    isAxiom((expression as Binary).right, (axiom as Binary).right, dict);
        }
    } else {
        return false;
    }
}

export function isAnyAxiom(expression: Expression) {
    return parsedAxioms.some((axiom) => isAxiom(expression, axiom));
}