import {readFileSync, WriteStream} from "fs";
import * as path from "path";
import {Binary, Expression, Implication, Not, Var} from "./expressions";
import * as _ from "lodash";
import {isAnyAxiom} from "./axioms";
import {Parser} from "./parser";

export class Proof {
    public expression: Expression;
    public assumptions: Expression[];
    public expressions: Expression[];

    constructor(expression: Expression, assumptions: Array<Expression>) {
        this.expression = expression;
        this.assumptions = assumptions;
        this.expressions = [];
    }

    public deduction() {
        let newExpressions = [];
        _.each(this.expressions, (expression, idx) => {
            if (expression.valueOf() === _.first(this.assumptions).valueOf()) {
                _.each(proofs.AtoA, (proofLine) => newExpressions.push(Parser.createExpression(proofLine, {A: expression})));
            } else if (isAnyAxiom(expression) || this.isAssumption(expression)) {
                newExpressions.push(expression);
                newExpressions.push(Parser.createExpression("A->B->A", {A: expression, B: _.first(this.assumptions)}));
                newExpressions.push(Parser.createExpression("B->A", {A: expression, B: _.first(this.assumptions)}));
            } else { // M.P.
                for(let j = idx - 1; j >= 0; j--) {
                    let tmp = new Implication(this.expressions[j], expression);
                    if (this.expressions.some((expression) => expression.valueOf() === tmp.valueOf())) {
                        let substitution = {
                            A: _.first(this.assumptions),
                            B: this.expressions[j],
                            C: expression,
                            D: tmp
                        };

                        newExpressions.push(Parser.createExpression("(A->B)->(A->D)->(A->C)", substitution));
                        newExpressions.push(Parser.createExpression("(A->D)->(A->C)", substitution));
                        newExpressions.push(Parser.createExpression("A->C", substitution));
                        break;
                    }
                }
            }
        });

        this.expression = new Implication(_.first(this.assumptions), this.expression);
        this.assumptions.shift();
        this.expressions = newExpressions;
    }

    public merge(other: Proof) {
        let substitution = {
            A: _.first(this.assumptions),
            B: _.first(other.assumptions),
            C: this.expression,
            D: null,
            E: null
        };

        this.deduction();
        other.deduction();

        substitution.D = this.expression;
        substitution.E = other.expression;

        this.expressions = this.expressions.concat(other.expressions);
        this.expressions.push(Parser.createExpression("D->E->(A|B->C)", substitution));
        this.expressions.push(Parser.createExpression("E->(A|B->C)", substitution));
        this.expressions.push(Parser.createExpression("(A|B->C)", substitution));
        _.map(proofs["Aor!A"], (proofLine) => this.expressions.push(Parser.createExpression(proofLine, substitution)));
        this.expressions.push(substitution.C);
        this.expression = _.last(this.expressions);

        return this;
    }

    private isAssumption(expression: Expression) {
        return this.assumptions.some((assumption) => assumption.valueOf() === expression.valueOf());
    }

    public print(ws: WriteStream) {
        ws.write(`${_.map(this.assumptions, (assumption) => assumption.toString()).join(', ')}`);
        ws.write(`|- ${this.expression.toString()}\n`);
        ws.write(`${_.map(this.expressions, (expression) => expression.toString()).join('\n')}\n`);
    }

    static createProof(expression: Expression, assumptions: Array<Expression>, proof: Proof = new Proof(expression, assumptions)) : Proof {
        const helper = (expression) => {
            if (expression instanceof Var) {
                if (proof.isAssumption(expression)) {
                    proof.expressions.push(expression);
                    return true;
                } else {
                    proof.expressions.push(new Not(expression));
                    return false;
                }
            } else if (expression instanceof Not) {
                let tmp = helper(expression.val);
                if (tmp) {
                    _.map(proofs["Ato!!A"], (proofLine) => proof.expressions.push(Parser.createExpression(proofLine, {A: expression.val})));
                }

                return !tmp;
            } else if (expression instanceof Binary) {
                let A = helper(expression.left);
                let B = helper(expression.right);
                _.map(proofs[expression.constructor.name][A][B], (proofLine) => proof.expressions.push(Parser.createExpression(proofLine, {A: expression.left, B: expression.right})));
                return _.last(proof.expressions).valueOf() ===
                    expression.valueOf();
            } else {
                return false;
            }
        };

        helper(expression);

        return proof;
    }
}


const proofs = {
    Implication: {
        false: {
            true: readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Implication', '!AB'), 'utf8').split('\n'),
            false: readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Implication', '!A!B'), 'utf8').split('\n')
        },
        true: {
            true: readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Implication', 'AB'), 'utf8').split('\n'),
            false: readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Implication', 'A!B'), 'utf8').split('\n')
        }
    },
    Conjuction: {
        false: {
            true: readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Conjuction', '!AB'), 'utf8').split('\n'),
            false: readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Conjuction', '!A!B'), 'utf8').split('\n')
        },
        true: {
            true: readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Conjuction', 'AB'), 'utf8').split('\n'),
            false: readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Conjuction', 'A!B'), 'utf8').split('\n')
        }
    },
    Disjuction: {
        false: {
            true: readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Disjunction', '!AB'), 'utf8').split('\n'),
            false: readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Disjunction', '!A!B'), 'utf8').split('\n')
        },
        true: {
            true: readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Disjunction', 'AB'), 'utf8').split('\n'),
            false: readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Disjunction', 'A!B'), 'utf8').split('\n')
        }
    },
    AtoA: readFileSync(path.join(__dirname, '..','hw3proofs', 'A_Implication_A'), 'utf8').split('\n'),
    'Aor!A': readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Aor!A'), 'utf8').split('\n'),
    'Ato!!A': readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Ato!!A'), 'utf8').split('\n')
};