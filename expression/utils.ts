import {Any, Binary, Expression, Predicate, Unary} from "./expressions";
import * as _ from 'lodash';

export const mod = 1e9 + 9;

export function hashString(string: string) {
    let hash = 0, i, chr;
    if (string.length === 0) return hash;
    for (i = 0; i < string.length; i++) {
        chr = string.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0;
    }
    return hash;
}

export function isDigit(char) {
    return char && !/[^\d]/.test(char);
}

export function isAlpha(char) {
    return char && /^[a-zA-Z]$/i.test(char);
}

export function isLowerString(string: string) {
    return string && string === string.toLowerCase();
}

export function isUpperString(string: string) {
    return string && string === string.toUpperCase();
}

export function exportVariables(exp: Expression, dict = {}) {
    if (exp.constructor.name === 'Var') {
        if (!dict[exp.toString()])
            dict[exp.toString()] = Object.keys(dict).length;
    } else if (exp instanceof Unary) {
        exportVariables(exp.val, dict);
    } else {
        exportVariables((exp as Binary).left, dict);
        exportVariables((exp as Binary).right, dict);
    }

    return dict;
}

export function exportFreeVariables(exp: Expression, dict = {}, result: Set<any> = new Set()) {
    if (exp.constructor.name === 'Var') {
        if (!dict[exp.toString()]) {
            result.add(exp.toString());
        }
    } else if (exp.constructor.name === 'Predicate') {
        _.each((exp as Predicate).val, val => exportFreeVariables(val, dict, result));
    } else if (exp.constructor.name === 'Any' || exp.constructor.name === 'Exists') {
        let tempExp = (exp as Any);
        if (dict[tempExp.var.toString()])
            dict[tempExp.var.toString()]++;
        else
            dict[tempExp.var.toString()] = 1;

        exportFreeVariables(tempExp.val, dict, result);
        dict[tempExp.var.toString()]--;
        if (dict[tempExp.var.toString()] === 0)
            delete dict[tempExp.var.toString()];
    } else if (exp instanceof Unary) {
        exportFreeVariables(exp.val, dict, result);
    } else {
        let tempExp = (exp as Binary);
        exportFreeVariables(tempExp.left, dict, result);
        exportFreeVariables(tempExp.right, dict, result);
    }

    return result;
}

export function checkTautalogy(exp: Expression) {
    let tmp = exportVariables(exp);
    let dictionary = {};
    let size = Object.keys(tmp).length;
    let combos = [];
    Object.keys(tmp).map((variable, id) => {dictionary[id] = variable;});

    for(let i = 0; i < Math.pow(2, size); i++) {
        let variables = {};
        for(let j = 0; j < size; j++) {
            variables[dictionary[j]] = Boolean(i & Math.pow(2, j));
        }

        combos.push(variables);
        if (!exp.eval(variables)) {
            return {status: true, ans: variables};
        }
    }

    return {status: false, ans: [], variables: dictionary, combos};
}