import {isAlpha, isDigit} from "./utils";
import * as _ from 'lodash';
import {Binary, Conjuction, Disjuction, Expression, Implication, Not, Unary, Var} from "./expressions";

type Tokens = Array<string>;

interface ParseResult {
    result: Expression;
    trail: Array<string>
}

export class Parser {
    public static normalizeString(string: string) {
        let result = _.map(string, (char) => {
            if (isDigit(char) || isAlpha(char)) {
                return char;
            } else if (char === '-') {
                return " -";
            } else if (char === '>') {
                return "> ";
            } else {
                return ` ${char} `;
            }
        });

        return result.join('');
    }

    public static parseExp(string: string) : Expression {
        let input = this.normalizeString(string);
        input = _.trimEnd(_.trimStart(input));
        let tokens = input.split(/\s+/g);
        let {result} = this.parseImplication(tokens);
        return result;
    }

    public static parseImplication(tokens: Tokens) : ParseResult {
        let {result, trail} = this.parseDisjuction(tokens);
        while (_.first(trail) === '->') {
            let tmp = this.parseImplication(trail.slice(1));
            trail = tmp.trail;
            result = new Implication(result, tmp.result);
        }

        return {result, trail}
    }

    public static parseDisjuction(tokens: Tokens) : ParseResult {
        let {result, trail} = this.parseConjuction(tokens);
        while (_.first(trail) === '|') {
            let tmp = this.parseConjuction(trail.slice(1));
            trail = tmp.trail;
            result = new Disjuction(result, tmp.result);
        }

        return {result, trail}
    }

    public static parseConjuction(tokens: Tokens) : ParseResult {
        let {result, trail} = this.parseNot(tokens);
        while (_.first(trail) === '&') {
            let tmp = this.parseNot(trail.slice(1));
            trail = tmp.trail;
            result = new Conjuction(result, tmp.result);
        }

        return {result, trail}
    }

    public static parseNot(tokens: Tokens) : ParseResult {
        if (_.first(tokens) === '!') {
            let tmp = this.parseNot(tokens.slice(1));
            let result = new Not(tmp.result);
            return {result, trail: tmp.trail};
        } else {
            return this.parseUnary(tokens);
        }
    }

    public static parseUnary(tokens: Tokens) : ParseResult {
        if (_.first(tokens) === '(') {
            let tmp = this.parseImplication(tokens.slice(1));
            return {result: tmp.result, trail: tmp.trail.slice(1)};
        } else {
            let tmp = new Var(tokens[0]);
            return {result: tmp, trail: tokens.slice(1)};
        }
    }

    static createExpression(string: string, dict) {
        const substitute = (expression: Expression, dict) => {
            if (expression instanceof Var) {
                return dict[expression.val];
            } else if (expression instanceof Unary) {
                expression.val = substitute(expression.val, dict);
            } else {
                let tmp = (expression as Binary);
                tmp.left = substitute(tmp.left, dict);
                tmp.right = substitute(tmp.right, dict);
            }

            expression.updateHash();
            return expression;
        };


        return substitute(Parser.parseExp(string), dict);
    };
}