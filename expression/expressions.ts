import {hashString, mod} from "./utils";
import * as _ from 'lodash';

export class Expression {
    protected hash: number;

    protected constructor () {
        this.hash = 0
    }

    public valueOf() {
        return this.hash;
    }

    public updateHash() {
        this.hash = 0;
    }

    public eval(dict) {
        return false;
    }
}

export class Unary extends Expression {
    public val: any;

    protected constructor(value) {
        super();
        this.val = value;
    }

    public updateHash() {
        this.hash = hashString(this.toString());
    }
}

export class Var extends Unary {

    constructor(value) {
        super(value);
        this.updateHash();
    }

    public updateHash() {
        this.hash = hashString(this.toString());
    }

    public toString() {
        return this.val.toString();
    }

    public eval(dict) {
        return dict[this.val];
    }
}

export class Any extends Unary {
    public readonly name: string = "@";
    public var: Unary;

    constructor(variable, value) {
        super(value);
        this.var = variable;
        this.updateHash();
    }

    public toString() {
        return `(@${this.var}${this.val.toString()})`;
    }
}

export class Exists extends Unary {
    public readonly name: string = "?";
    public var: Unary;

    constructor(variable, value) {
        super(value);
        this.var = variable;
        this.updateHash();
    }

    public toString() {
        return `(?${this.var}${this.val.toString()})`;
    }
}

export class Predicate extends Unary {
    public name: string;

    constructor(name, values) {
        super(values);
        this.name = name;
        this.updateHash();
    }

    public toString() {
        if (_.size(this.val) == 0) {
            return this.name;
        }

        return this.name + "(" + this.val.map(val => val.toString()).join(',') + ")";
    }
}

export class Next extends Unary {
    public readonly name: string = "'";

    constructor(value) {
        super(value);
        this.updateHash();
    }

    public toString() {
        return this.val.toString() + this.name;
    }
}

export class Not extends Unary {
    public readonly name: string = '!';

    constructor(value) {
        super(value);
        this.updateHash();
    }

    public toString() {
        return `(!${this.val.toString()})`;
    }

    public eval(dict) {
        return !this.val.eval(dict);
    }
}

export class Binary extends Expression {
    public readonly name: string;
    public left: Expression;
    public right: Expression;

    protected constructor(left, right) {
        super();
        this.left = left;
        this.right = right;
    }

    public valueOf() {
        if (!this.hash) {
            this.updateHash();
        }


        return this.hash;
    }

    public updateHash() {
        this.hash = hashString(this.toString());
    }

    public toString() {
        return `(${this.left.toString()}${this.name}${this.right.toString()})`;
    }

    public calc(left, right) {
        return false;
    }

    public eval(dict) {
        return this.calc(this.left.eval(dict), this.right.eval(dict));
    }
}

export class Implication extends Binary {
    public readonly name: string = "->";

    constructor(left, right) {
        super(left, right);
        this.updateHash();
    }

    public calc(left, right) {
        return (!left) || right;
    }
}

export class Conjuction extends Binary {
    public readonly name: string = "&";

    constructor(left, right) {
        super(left, right);
        this.updateHash();
    }

    public calc(left, right) {
        return left && right;
    }
}

export class Disjuction extends Binary {
    public readonly name: string = "|";

    constructor(left, right) {
        super(left, right);
        this.updateHash();
    }

    public calc(left, right) {
        return left || right;
    }
}

export class Equals extends Binary {
    public readonly name: string = "=";

    constructor(left, right) {
        super(left, right);
    }
}

export class Sum extends Binary {
    public readonly name: string = "+";

    constructor(left, right) {
        super(left, right);
    }
}

export class Mul extends Binary {
    public readonly name: string = "*";

    constructor(left, right) {
        super(left, right);
    }
}
