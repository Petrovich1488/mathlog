"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const path = require("path");
const expressions_1 = require("./expressions");
const _ = require("lodash");
const axioms_1 = require("./axioms");
const parser_1 = require("./parser");
class Proof {
    constructor(expression, assumptions) {
        this.expression = expression;
        this.assumptions = assumptions;
        this.expressions = [];
    }
    deduction() {
        let newExpressions = [];
        _.each(this.expressions, (expression, idx) => {
            if (expression.valueOf() === _.first(this.assumptions).valueOf()) {
                _.each(proofs.AtoA, (proofLine) => newExpressions.push(parser_1.Parser.createExpression(proofLine, { A: expression })));
            }
            else if (axioms_1.isAnyAxiom(expression) || this.isAssumption(expression)) {
                newExpressions.push(expression);
                newExpressions.push(parser_1.Parser.createExpression("A->B->A", { A: expression, B: _.first(this.assumptions) }));
                newExpressions.push(parser_1.Parser.createExpression("B->A", { A: expression, B: _.first(this.assumptions) }));
            }
            else { // M.P.
                for (let j = idx - 1; j >= 0; j--) {
                    let tmp = new expressions_1.Implication(this.expressions[j], expression);
                    if (this.expressions.some((expression) => expression.valueOf() === tmp.valueOf())) {
                        let substitution = {
                            A: _.first(this.assumptions),
                            B: this.expressions[j],
                            C: expression,
                            D: tmp
                        };
                        newExpressions.push(parser_1.Parser.createExpression("(A->B)->(A->D)->(A->C)", substitution));
                        newExpressions.push(parser_1.Parser.createExpression("(A->D)->(A->C)", substitution));
                        newExpressions.push(parser_1.Parser.createExpression("A->C", substitution));
                        break;
                    }
                }
            }
        });
        this.expression = new expressions_1.Implication(_.first(this.assumptions), this.expression);
        this.assumptions.shift();
        this.expressions = newExpressions;
    }
    merge(other) {
        let substitution = {
            A: _.first(this.assumptions),
            B: _.first(other.assumptions),
            C: this.expression,
            D: null,
            E: null
        };
        this.deduction();
        other.deduction();
        substitution.D = this.expression;
        substitution.E = other.expression;
        this.expressions = this.expressions.concat(other.expressions);
        this.expressions.push(parser_1.Parser.createExpression("D->E->(A|B->C)", substitution));
        this.expressions.push(parser_1.Parser.createExpression("E->(A|B->C)", substitution));
        this.expressions.push(parser_1.Parser.createExpression("(A|B->C)", substitution));
        _.map(proofs["Aor!A"], (proofLine) => this.expressions.push(parser_1.Parser.createExpression(proofLine, substitution)));
        this.expressions.push(substitution.C);
        this.expression = _.last(this.expressions);
        return this;
    }
    isAssumption(expression) {
        return this.assumptions.some((assumption) => assumption.valueOf() === expression.valueOf());
    }
    print(ws) {
        ws.write(`${_.map(this.assumptions, (assumption) => assumption.toString()).join(', ')}`);
        ws.write(`|- ${this.expression.toString()}\n`);
        ws.write(`${_.map(this.expressions, (expression) => expression.toString()).join('\n')}\n`);
    }
    static createProof(expression, assumptions, proof = new Proof(expression, assumptions)) {
        const helper = (expression) => {
            if (expression instanceof expressions_1.Var) {
                if (proof.isAssumption(expression)) {
                    proof.expressions.push(expression);
                    return true;
                }
                else {
                    proof.expressions.push(new expressions_1.Not(expression));
                    return false;
                }
            }
            else if (expression instanceof expressions_1.Not) {
                let tmp = helper(expression.val);
                if (tmp) {
                    _.map(proofs["Ato!!A"], (proofLine) => proof.expressions.push(parser_1.Parser.createExpression(proofLine, { A: expression.val })));
                }
                return !tmp;
            }
            else if (expression instanceof expressions_1.Binary) {
                let A = helper(expression.left);
                let B = helper(expression.right);
                _.map(proofs[expression.constructor.name][A][B], (proofLine) => proof.expressions.push(parser_1.Parser.createExpression(proofLine, { A: expression.left, B: expression.right })));
                return _.last(proof.expressions).valueOf() ===
                    expression.valueOf();
            }
            else {
                return false;
            }
        };
        helper(expression);
        return proof;
    }
}
exports.Proof = Proof;
const proofs = {
    Implication: {
        false: {
            true: fs_1.readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Implication', '!AB'), 'utf8').split('\n'),
            false: fs_1.readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Implication', '!A!B'), 'utf8').split('\n')
        },
        true: {
            true: fs_1.readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Implication', 'AB'), 'utf8').split('\n'),
            false: fs_1.readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Implication', 'A!B'), 'utf8').split('\n')
        }
    },
    Conjuction: {
        false: {
            true: fs_1.readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Conjuction', '!AB'), 'utf8').split('\n'),
            false: fs_1.readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Conjuction', '!A!B'), 'utf8').split('\n')
        },
        true: {
            true: fs_1.readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Conjuction', 'AB'), 'utf8').split('\n'),
            false: fs_1.readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Conjuction', 'A!B'), 'utf8').split('\n')
        }
    },
    Disjuction: {
        false: {
            true: fs_1.readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Disjunction', '!AB'), 'utf8').split('\n'),
            false: fs_1.readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Disjunction', '!A!B'), 'utf8').split('\n')
        },
        true: {
            true: fs_1.readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Disjunction', 'AB'), 'utf8').split('\n'),
            false: fs_1.readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Disjunction', 'A!B'), 'utf8').split('\n')
        }
    },
    AtoA: fs_1.readFileSync(path.join(__dirname, '..', 'hw3proofs', 'A_Implication_A'), 'utf8').split('\n'),
    'Aor!A': fs_1.readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Aor!A'), 'utf8').split('\n'),
    'Ato!!A': fs_1.readFileSync(path.join(__dirname, '..', 'hw3proofs', 'Ato!!A'), 'utf8').split('\n')
};
//# sourceMappingURL=proof.js.map