import {FormalParser} from "./formalParser";
import {Any, Binary, Exists, Expression, Implication, Predicate, Unary, Var} from "./expressions";
import * as _ from "lodash";
import {exportFreeVariables} from "./utils";
import {isAxiom, parsedAxioms} from "./axioms";

const formalAxiomsRaw = [
    "a=b->a'=b'",
    "a=b->a=c->b=c",
    "a'=b'->a=b",
    "!(a'=0)",
    "a+b'=(a+b)'",
    "a+0=a",
    "a*0=0",
    "a*b'=a*b+a"
];

let formalParser = new FormalParser();
export const formalAxioms:Array<Expression> = _.map(formalAxiomsRaw, (rawAxiom) => formalParser.parseExpr(rawAxiom));

export function freeSubtract(template: Expression, exp: Expression, variable, locked = {}, dict = {}) {
    if (template.constructor.name === 'Var') {
        if (template.valueOf() !== variable.valueOf() || locked[(template as Unary).val.valueOf()]) {
            return template.valueOf() === exp.valueOf();
        } else {
            let temp = new Set<any>();
            exportFreeVariables(exp, {}, temp);
            if (_.size(_.intersection(Array.from(temp), Object.keys(locked))) !== 0) {
                return false;
            }

            dict[template.valueOf()] = exp;
            return true;
        }
    } else if (template.constructor.name === exp.constructor.name) {
        if (template.constructor.name === 'Any' || template.constructor.name === 'Exists') {
            let temptemplate = (template as Any);
            locked[temptemplate.var.val.valueOf()] = (locked[temptemplate.var.val.valueOf()] || 0) + 1;

            let result = freeSubtract(temptemplate.val, (exp as Unary).val, variable, locked, dict);
            locked[temptemplate.var.val.valueOf()]--;
            if (locked[temptemplate.var.val.valueOf()] === 0) {
                delete locked[temptemplate.var.val.valueOf()];
            }

            return result;
        } else if (template.constructor.name === 'Predicate') {
            let temptemplate = (template as Predicate);
            let tempexp = (exp as Predicate);

            if (_.size(temptemplate.val) !== _.size(tempexp.val)) {
                return false;
            }

            for (let i = 0; i < _.size(temptemplate.val); i++) {
                if (!freeSubtract(temptemplate.val[i], tempexp.val[i], variable, locked, dict))
                    return false;
            }

            return true;
        } else if (template instanceof Unary) {
            return freeSubtract(template.val, (exp as Unary).val, variable, locked, dict);
        } else {
            let temptemplate = (template as Binary);
            let tempexp = (exp as Binary);

            if (!freeSubtract(temptemplate.left, tempexp.left, variable, locked, dict)) {
                return false;
            } else return freeSubtract(temptemplate.right, tempexp.right, variable, locked, dict);
        }
    } else return false;
}

export function isAxiomAny(expr: Expression) {
    if (!(expr instanceof Implication) || !((expr as Implication).left instanceof Any)) {
        return false;
    }

    let aexpr = (expr as Implication);
    let aleft = (aexpr.left as Any);
    return freeSubtract(aleft.val, aexpr.right, aleft.var, {}, {});
}

export function isAxiomExists(expr: Expression) {
    if (!(expr instanceof Implication) || !((expr as Implication).right instanceof Exists)) {
        return false;
    }

    let aexpr = (expr as Implication);
    let aright = (aexpr.right as Exists);
    return freeSubtract(aright.val, aexpr.left, aright.var, {}, {});
}

export function isFormalAxiom(axiom, expression, dict = {}, locked = new Set()) {
    if (axiom.constructor.name === Var.name) {
        if (locked.has(axiom)) {
            return axiom.valueOf() === expression.valueOf();
        } else if (!_.isNil(dict[axiom.valueOf()])) {
            return dict[axiom.valueOf()].valueOf() === expression.valueOf();
        } else {
            dict[axiom.valueOf()] = expression;
            return true;
        }
    } else if (expression.constructor.name === axiom.constructor.name) {
        if (axiom instanceof Any || axiom instanceof Exists) {
            locked.add(axiom.var);
            let result = isFormalAxiom(axiom.val, expression.val, dict, locked);
            locked.delete(axiom.var);

            return result;
        } else if (axiom instanceof Predicate) {
            return _.size(axiom.val) === _.size(expression.val) && axiom.val.every((val, index) => isFormalAxiom(axiom.val[index], expression.val[index], dict, locked));
        } else if (axiom instanceof Unary) {
            return isFormalAxiom(expression.val, axiom.val, dict);
        } else {
            return  isFormalAxiom(expression.left, axiom.left, dict) &&
                    isFormalAxiom(expression.right, axiom.right, dict);
        }
    } else {
        return false;
    }
}

export function isAnyFormalAxiom(expr: Expression) {
    return  formalAxioms.some((axiom) => isFormalAxiom(axiom, expr)) ||
            (isAxiomAny(expr) || isAxiomExists(expr));
}