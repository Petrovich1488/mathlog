"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("./utils");
const _ = require("lodash");
const expressions_1 = require("./expressions");
class Parser {
    static normalizeString(string) {
        let result = _.map(string, (char) => {
            if (utils_1.isDigit(char) || utils_1.isAlpha(char)) {
                return char;
            }
            else if (char === '-') {
                return " -";
            }
            else if (char === '>') {
                return "> ";
            }
            else {
                return ` ${char} `;
            }
        });
        return result.join('');
    }
    static parseExp(string) {
        let input = this.normalizeString(string);
        input = _.trimEnd(_.trimStart(input));
        let tokens = input.split(/\s+/g);
        let { result } = this.parseImplication(tokens);
        return result;
    }
    static parseImplication(tokens) {
        let { result, trail } = this.parseDisjuction(tokens);
        while (_.first(trail) === '->') {
            let tmp = this.parseImplication(trail.slice(1));
            trail = tmp.trail;
            result = new expressions_1.Implication(result, tmp.result);
        }
        return { result, trail };
    }
    static parseDisjuction(tokens) {
        let { result, trail } = this.parseConjuction(tokens);
        while (_.first(trail) === '|') {
            let tmp = this.parseConjuction(trail.slice(1));
            trail = tmp.trail;
            result = new expressions_1.Disjuction(result, tmp.result);
        }
        return { result, trail };
    }
    static parseConjuction(tokens) {
        let { result, trail } = this.parseNot(tokens);
        while (_.first(trail) === '&') {
            let tmp = this.parseNot(trail.slice(1));
            trail = tmp.trail;
            result = new expressions_1.Conjuction(result, tmp.result);
        }
        return { result, trail };
    }
    static parseNot(tokens) {
        if (_.first(tokens) === '!') {
            let tmp = this.parseNot(tokens.slice(1));
            let result = new expressions_1.Not(tmp.result);
            return { result, trail: tmp.trail };
        }
        else {
            return this.parseUnary(tokens);
        }
    }
    static parseUnary(tokens) {
        if (_.first(tokens) === '(') {
            let tmp = this.parseImplication(tokens.slice(1));
            return { result: tmp.result, trail: tmp.trail.slice(1) };
        }
        else {
            let tmp = new expressions_1.Var(tokens[0]);
            return { result: tmp, trail: tokens.slice(1) };
        }
    }
    static createExpression(string, dict) {
        const substitute = (expression, dict) => {
            if (expression instanceof expressions_1.Var) {
                return dict[expression.val];
            }
            else if (expression instanceof expressions_1.Unary) {
                expression.val = substitute(expression.val, dict);
            }
            else {
                let tmp = expression;
                tmp.left = substitute(tmp.left, dict);
                tmp.right = substitute(tmp.right, dict);
            }
            expression.updateHash();
            return expression;
        };
        return substitute(Parser.parseExp(string), dict);
    }
    ;
}
exports.Parser = Parser;
//# sourceMappingURL=parser.js.map