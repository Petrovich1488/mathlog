import {isAlpha, isDigit, isLowerString, isUpperString} from "./utils";
import * as _ from 'lodash';
import {
    Any,
    Conjuction,
    Disjuction,
    Equals,
    Exists,
    Implication,
    Mul,
    Not,
    Predicate,
    Sum,
    Var,
    Next,
    Expression, Unary, Binary
} from "./expressions";

export class FormalParser {
    public string: string;
    public index: number;

    constructor() {
        this.string = "";
        this.index = 0;
    }

    public parseExpr(string: string) {
        this.index = 0;
        this.string = string;

        return this.parseImplication();
    }

    public parse() {
        if (this.index >= _.size(this.string)) {
            return null;
        }

        return this.parseImplication();
    }

    private readVarName() {
        let j = this.index;
        while (j < _.size(this.string) && (isDigit(this.string[j]) || (isAlpha(this.string[j]) && isLowerString(this.string[j])))) {
            j++;
        }

        let result = this.string.substring(this.index, j);
        this.index = j;

        return result;
    }

    private readPredicateName() {
        let j = this.index;
        if (!(isAlpha(this.string[j]) && isUpperString(this.string[j]))) {
            return "";
        }

        while (j < _.size(this.string) && (isDigit(this.string[j]) || (isAlpha(this.string[j]) && isUpperString(this.string[j])))) {
            j++;
        }

        let result = this.string.substring(this.index, j);
        this.index = j;

        return result;
    }

    private parseImplication() {
        let result = this.parseDisjuction();
        if (this.index < _.size(this.string) && this.string[this.index] == '-') {
            this.index += 2;
            let temp = this.parseImplication();
            return new Implication(result, temp);
        }

        return result;
    }

    private parseDisjuction() {
        let result = this.parseConjuction();
        while (this.index < _.size(this.string) && this.string[this.index] == '|') {
            this.index += 1;
            let temp = this.parseConjuction();
            result = new Disjuction(result, temp);
        }

        return result;
    }

    private parseConjuction() {
        let result = this.parseUnary();
        while (this.index < _.size(this.string) && this.string[this.index] == '&') {
            this.index += 1;
            let temp = this.parseUnary();
            result = new Conjuction(result, temp);
        }

        return result;
    }

    private parseUnary() {
        if (this.string[this.index] === '!') {
            this.index += 1;
            let temp = this.parseUnary();
            return new Not(temp);
        } else if(this.string[this.index] === '@' || this.string[this.index] === '?') {
            let symbol = this.string[this.index];
            this.index++;
            let word = this.readVarName();
            let temp = this.parseUnary();
            if (symbol === "@")
                return new Any(new Var(word), temp);
            else return new Exists(new Var(word), temp);
        }

        let result = this.parsePredicate();
        if (result) return result;

        if (this.index < _.size(this.string) && this.string[this.index] === '(') {
            this.index++;
            result = this.parseImplication();
            this.index++;
            return result;
        }

        return new Var(this.readVarName());
    }

    private parsePredicate() {
        let word = this.readPredicateName();
        if (_.size(word)) {
            let args = this.parseArguments();
            return new Predicate(word, args);
        } else {
            let save = this.index;
            let result = this.parseTerm();
            if (this.index >= _.size(this.string) || this.string[this.index] !== '=') {
                this.index = save;
                return null;
            }

            this.index++;
            return new Equals(result, this.parseTerm());
        }
    }

    private parseArguments() {
        let result = [];
        if (this.index >= _.size(this.string) || this.string[this.index] !== '(') {
            return result;
        }

        this.index++;
        result.push(this.parseTerm());
        while (this.index < _.size(this.string) && this.string[this.index] !== ')') {
            this.index ++;
            result.push(this.parseTerm());
        }

        this.index++;
        return result;
    }

    private parseTerm() {
        let result = this.parseSum();
        while (this.index < _.size(this.string) && this.string[this.index] === '+') {
            this.index++;
            let temp = this.parseSum();
            result = new Sum(result, temp);
        }

        return result;
    }

    private parseSum() {
        let result = this.parseMul();
        while (this.index < _.size(this.string) && this.string[this.index] === '*') {
            this.index++;
            let temp = this.parseMul();
            result = new Mul(result, temp);
        }

        return result;
    }

    private parseMul() {
        let result = null;
        if (this.index < _.size(this.string) && this.string[this.index] == '(') {
            this.index++;
            result = this.parseTerm();
            this.index++;
            return this.parseNext(result);
        }

        let word = this.readVarName();
        if (this.index < _.size(this.string) && this.string[this.index] == '(') {
            let values = this.parseArguments();
            result = new Predicate(word, values);
        } else {
            result = new Var(word);
        }

        return this.parseNext(result);
    }

    private parseNext(value) {
        while (this.index < _.size(this.string) && this.string[this.index] == "'") {
            this.index++;
            value = new Next(value);
        }

        return value;
    }

    createExpression(string: string, dict) {
        const substitute = (expression: Expression, dict) => {
            if (expression instanceof Var) {
                return dict[expression.val];
            } else if (expression instanceof Predicate) {
                if (!_.isNil(dict[expression.name])) {
                    return dict[expression.name];
                }

                for(let i = 0; i < _.size(expression.val); i++) {
                    expression.val[i] = substitute(expression.val[i], dict);
                }
            } else if (expression instanceof Unary) {
                expression.val = substitute(expression.val, dict);
            } else {
                let tmp = (expression as Binary);
                tmp.left = substitute(tmp.left, dict);
                tmp.right = substitute(tmp.right, dict);
            }

            if ((expression instanceof Any || expression instanceof Exists) && expression.var.val === "x")
                expression.var = dict["x"].val;

            expression.updateHash();
            return expression;
        };


        return substitute(this.parseExpr(string), dict);
    };
}