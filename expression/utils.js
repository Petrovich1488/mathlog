"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const expressions_1 = require("./expressions");
const _ = require("lodash");
exports.mod = 1e9 + 9;
function hashString(string) {
    let hash = 0, i, chr;
    if (string.length === 0)
        return hash;
    for (i = 0; i < string.length; i++) {
        chr = string.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0;
    }
    return hash;
}
exports.hashString = hashString;
function isDigit(char) {
    return char && !/[^\d]/.test(char);
}
exports.isDigit = isDigit;
function isAlpha(char) {
    return char && /^[a-zA-Z]$/i.test(char);
}
exports.isAlpha = isAlpha;
function isLowerString(string) {
    return string && string === string.toLowerCase();
}
exports.isLowerString = isLowerString;
function isUpperString(string) {
    return string && string === string.toUpperCase();
}
exports.isUpperString = isUpperString;
function exportVariables(exp, dict = {}) {
    if (exp.constructor.name === 'Var') {
        if (!dict[exp.toString()])
            dict[exp.toString()] = Object.keys(dict).length;
    }
    else if (exp instanceof expressions_1.Unary) {
        exportVariables(exp.val, dict);
    }
    else {
        exportVariables(exp.left, dict);
        exportVariables(exp.right, dict);
    }
    return dict;
}
exports.exportVariables = exportVariables;
function exportFreeVariables(exp, dict = {}, result = new Set()) {
    if (exp.constructor.name === 'Var') {
        if (!dict[exp.toString()]) {
            result.add(exp.toString());
        }
    }
    else if (exp.constructor.name === 'Predicate') {
        _.each(exp.val, val => exportFreeVariables(val, dict, result));
    }
    else if (exp.constructor.name === 'Any' || exp.constructor.name === 'Exists') {
        let tempExp = exp;
        if (dict[tempExp.var.toString()])
            dict[tempExp.var.toString()]++;
        else
            dict[tempExp.var.toString()] = 1;
        exportFreeVariables(tempExp.val, dict, result);
        dict[tempExp.var.toString()]--;
        if (dict[tempExp.var.toString()] === 0)
            delete dict[tempExp.var.toString()];
    }
    else if (exp instanceof expressions_1.Unary) {
        exportFreeVariables(exp.val, dict, result);
    }
    else {
        let tempExp = exp;
        exportFreeVariables(tempExp.left, dict, result);
        exportFreeVariables(tempExp.right, dict, result);
    }
    return result;
}
exports.exportFreeVariables = exportFreeVariables;
function checkTautalogy(exp) {
    let tmp = exportVariables(exp);
    let dictionary = {};
    let size = Object.keys(tmp).length;
    let combos = [];
    Object.keys(tmp).map((variable, id) => { dictionary[id] = variable; });
    for (let i = 0; i < Math.pow(2, size); i++) {
        let variables = {};
        for (let j = 0; j < size; j++) {
            variables[dictionary[j]] = Boolean(i & Math.pow(2, j));
        }
        combos.push(variables);
        if (!exp.eval(variables)) {
            return { status: true, ans: variables };
        }
    }
    return { status: false, ans: [], variables: dictionary, combos };
}
exports.checkTautalogy = checkTautalogy;
//# sourceMappingURL=utils.js.map