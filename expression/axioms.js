"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const parser_1 = require("./parser");
const _ = require("lodash");
const expressions_1 = require("./expressions");
const axioms = [
    "A -> (B -> A)",
    "(A -> B) -> (A -> B -> C) -> (A -> C)",
    "A -> B -> A & B",
    "A & B -> A",
    "A & B -> B",
    "A -> A | B",
    "A -> B | A",
    "(A -> B) -> (C -> B) -> (A | C -> B)",
    "(A -> B) -> (A -> !B) -> !A",
    "!!A -> A"
];
exports.parsedAxioms = _.map(axioms, (rawAxiom) => parser_1.Parser.parseExp(rawAxiom));
function isAxiom(expression, axiom, dict = {}) {
    if (axiom.constructor.name === expressions_1.Var.name) {
        if (!_.isNil(dict[axiom.valueOf()])) {
            return dict[axiom.valueOf()].valueOf() === expression.valueOf();
        }
        else {
            dict[axiom.valueOf()] = expression;
            return true;
        }
    }
    else if (expression.constructor.name === axiom.constructor.name) {
        if (axiom instanceof expressions_1.Unary) {
            return isAxiom(expression.val, axiom.val, dict);
        }
        else {
            return isAxiom(expression.left, axiom.left, dict) &&
                isAxiom(expression.right, axiom.right, dict);
        }
    }
    else {
        return false;
    }
}
exports.isAxiom = isAxiom;
function isAnyAxiom(expression) {
    return exports.parsedAxioms.some((axiom) => isAxiom(expression, axiom));
}
exports.isAnyAxiom = isAnyAxiom;
//# sourceMappingURL=axioms.js.map