"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utils_1 = require("./utils");
const _ = require("lodash");
const expressions_1 = require("./expressions");
class FormalParser {
    constructor() {
        this.string = "";
        this.index = 0;
    }
    parseExpr(string) {
        this.index = 0;
        this.string = string;
        return this.parseImplication();
    }
    parse() {
        if (this.index >= _.size(this.string)) {
            return null;
        }
        return this.parseImplication();
    }
    readVarName() {
        let j = this.index;
        while (j < _.size(this.string) && (utils_1.isDigit(this.string[j]) || (utils_1.isAlpha(this.string[j]) && utils_1.isLowerString(this.string[j])))) {
            j++;
        }
        let result = this.string.substring(this.index, j);
        this.index = j;
        return result;
    }
    readPredicateName() {
        let j = this.index;
        if (!(utils_1.isAlpha(this.string[j]) && utils_1.isUpperString(this.string[j]))) {
            return "";
        }
        while (j < _.size(this.string) && (utils_1.isDigit(this.string[j]) || (utils_1.isAlpha(this.string[j]) && utils_1.isUpperString(this.string[j])))) {
            j++;
        }
        let result = this.string.substring(this.index, j);
        this.index = j;
        return result;
    }
    parseImplication() {
        let result = this.parseDisjuction();
        if (this.index < _.size(this.string) && this.string[this.index] == '-') {
            this.index += 2;
            let temp = this.parseImplication();
            return new expressions_1.Implication(result, temp);
        }
        return result;
    }
    parseDisjuction() {
        let result = this.parseConjuction();
        while (this.index < _.size(this.string) && this.string[this.index] == '|') {
            this.index += 1;
            let temp = this.parseConjuction();
            result = new expressions_1.Disjuction(result, temp);
        }
        return result;
    }
    parseConjuction() {
        let result = this.parseUnary();
        while (this.index < _.size(this.string) && this.string[this.index] == '&') {
            this.index += 1;
            let temp = this.parseUnary();
            result = new expressions_1.Conjuction(result, temp);
        }
        return result;
    }
    parseUnary() {
        if (this.string[this.index] === '!') {
            this.index += 1;
            let temp = this.parseUnary();
            return new expressions_1.Not(temp);
        }
        else if (this.string[this.index] === '@' || this.string[this.index] === '?') {
            let symbol = this.string[this.index];
            this.index++;
            let word = this.readVarName();
            let temp = this.parseUnary();
            if (symbol === "@")
                return new expressions_1.Any(new expressions_1.Var(word), temp);
            else
                return new expressions_1.Exists(new expressions_1.Var(word), temp);
        }
        let result = this.parsePredicate();
        if (result)
            return result;
        if (this.index < _.size(this.string) && this.string[this.index] === '(') {
            this.index++;
            result = this.parseImplication();
            this.index++;
            return result;
        }
        return new expressions_1.Var(this.readVarName());
    }
    parsePredicate() {
        let word = this.readPredicateName();
        if (_.size(word)) {
            let args = this.parseArguments();
            return new expressions_1.Predicate(word, args);
        }
        else {
            let save = this.index;
            let result = this.parseTerm();
            if (this.index >= _.size(this.string) || this.string[this.index] !== '=') {
                this.index = save;
                return null;
            }
            this.index++;
            return new expressions_1.Equals(result, this.parseTerm());
        }
    }
    parseArguments() {
        let result = [];
        if (this.index >= _.size(this.string) || this.string[this.index] !== '(') {
            return result;
        }
        this.index++;
        result.push(this.parseTerm());
        while (this.index < _.size(this.string) && this.string[this.index] !== ')') {
            this.index++;
            result.push(this.parseTerm());
        }
        this.index++;
        return result;
    }
    parseTerm() {
        let result = this.parseSum();
        while (this.index < _.size(this.string) && this.string[this.index] === '+') {
            this.index++;
            let temp = this.parseSum();
            result = new expressions_1.Sum(result, temp);
        }
        return result;
    }
    parseSum() {
        let result = this.parseMul();
        while (this.index < _.size(this.string) && this.string[this.index] === '*') {
            this.index++;
            let temp = this.parseMul();
            result = new expressions_1.Mul(result, temp);
        }
        return result;
    }
    parseMul() {
        let result = null;
        if (this.index < _.size(this.string) && this.string[this.index] == '(') {
            this.index++;
            result = this.parseTerm();
            this.index++;
            return this.parseNext(result);
        }
        let word = this.readVarName();
        if (this.index < _.size(this.string) && this.string[this.index] == '(') {
            let values = this.parseArguments();
            result = new expressions_1.Predicate(word, values);
        }
        else {
            result = new expressions_1.Var(word);
        }
        return this.parseNext(result);
    }
    parseNext(value) {
        while (this.index < _.size(this.string) && this.string[this.index] == "'") {
            this.index++;
            value = new expressions_1.Next(value);
        }
        return value;
    }
    createExpression(string, dict) {
        const substitute = (expression, dict) => {
            if (expression instanceof expressions_1.Var) {
                return dict[expression.val];
            }
            else if (expression instanceof expressions_1.Predicate) {
                if (!_.isNil(dict[expression.name])) {
                    return dict[expression.name];
                }
                for (let i = 0; i < _.size(expression.val); i++) {
                    expression.val[i] = substitute(expression.val[i], dict);
                }
            }
            else if (expression instanceof expressions_1.Unary) {
                expression.val = substitute(expression.val, dict);
            }
            else {
                let tmp = expression;
                tmp.left = substitute(tmp.left, dict);
                tmp.right = substitute(tmp.right, dict);
            }
            if ((expression instanceof expressions_1.Any || expression instanceof expressions_1.Exists) && expression.var.val === "x")
                expression.var = dict["x"].val;
            expression.updateHash();
            return expression;
        };
        return substitute(this.parseExpr(string), dict);
    }
    ;
}
exports.FormalParser = FormalParser;
//# sourceMappingURL=formalParser.js.map