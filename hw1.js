"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const parser_1 = require("./expression/parser");
const _ = require("lodash");
const axioms_1 = require("./expression/axioms");
const expressions_1 = require("./expression/expressions");
const path = require("path");
const fout = fs_1.createWriteStream(path.join(__dirname, 'input', 'hw1.out'), {
    flags: 'w'
});
let input = _(fs_1.readFileSync(path.join(__dirname, 'input', 'hw1.in'), 'utf8'))
    .split('\n')
    .map((line) => { return _.trim(line); })
    .value();
let assumptions = [];
let expressions = [];
let exprToLine = {};
let line = 1;
const parseAssumptions = () => {
    let list = input[0].split('|-');
    if (_.size(list) === 2) {
        let rawAssumptions = list[0].split(',');
        _.each(rawAssumptions, (rawAssumption, idx) => {
            if (_.size(rawAssumption))
                assumptions[idx] = parser_1.Parser.parseExp(rawAssumption);
        });
    }
    fout.write(input[0] + '\n');
    input = input.slice(1);
};
const annotate = () => {
    _.each(input, (rawExpression) => {
        if (!_.size(rawExpression))
            return;
        let expression = parser_1.Parser.parseExp(rawExpression);
        let annotationInfo = { state: 0, num: -1 };
        let tryAssumption = _.findIndex(assumptions, (assumption) => assumption.valueOf() === expression.valueOf());
        if (tryAssumption !== -1) {
            annotationInfo = { state: 1, num: tryAssumption + 1 };
        }
        if (annotationInfo.state === 0)
            _.each(axioms_1.parsedAxioms, (axiom, idx) => {
                if (annotationInfo.state === 0)
                    if (axioms_1.isAxiom(expression, axiom))
                        annotationInfo = { state: 2, num: idx + 1 };
            });
        if (annotationInfo.state === 0) {
            _.each(expressions, (knownExpression) => {
                if (annotationInfo.state !== 0)
                    return;
                let tmp = new expressions_1.Implication(knownExpression, expression);
                let tryMP = exprToLine[tmp.valueOf()];
                if (!_.isNil(tryMP))
                    annotationInfo = { state: 3, num: [exprToLine[knownExpression.valueOf()], exprToLine[tmp.valueOf()]] };
            });
        }
        fout.write(`( ${line} ) ${rawExpression} `);
        switch (annotationInfo.state) {
            case 0:
                fout.write('(Не доказано)');
                break;
            case 1:
                fout.write(`(Предположение ${annotationInfo.num})`);
                break;
            case 2:
                fout.write(`(Сх. акс. ${annotationInfo.num})`);
                break;
            case 3:
                let [first, second] = annotationInfo.num;
                fout.write(`(M.P. ${first}, ${second})`);
                break;
            default: break;
        }
        fout.write('\n');
        if (annotationInfo.state !== 0) {
            expressions.push(expression);
            exprToLine[expression.valueOf()] = line;
        }
        line++;
    });
};
parseAssumptions();
annotate();
//# sourceMappingURL=hw1.js.map