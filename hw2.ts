import { readFileSync, createWriteStream } from "fs";
import {Parser} from "./expression/parser";
import * as _ from 'lodash';
import {isAxiom, parsedAxioms} from "./expression/axioms";
import {Expression, Implication} from "./expression/expressions";
import * as path from "path";
import * as os from "os";

const fout = createWriteStream(path.join(__dirname, 'input', 'hw2.out'), {
    flags: 'w'
});

let input = _(readFileSync(path.join(__dirname, 'input', 'hw2.in'), 'utf8'))
    .split('\n')
    .map((line) => { return _.trim(line); })
    .value();
let assumptions:Array<Expression> = [];
let expressions:Array<Expression> = [];
let exprToLine = {};
let lastAssumption = null;
let line = 1;

const processAssumptions = () => {
    let firstLine = _.first(input);
    input = input.slice(1);
    if (!_.size(firstLine))
        return;

    let list = firstLine.split('|-');
    let rawAssumptions = list[0].split(',');
    _.each(rawAssumptions, (rawAssumption, idx) => {
        if (_.size(rawAssumption)) {
            assumptions[idx] = Parser.parseExp(rawAssumption);
            lastAssumption = assumptions[idx];
        }
    });

    fout.write(`${_.initial(rawAssumptions).join(',')} |- (${_.last(rawAssumptions)}) -> (${list[1]})\n`);
};

const performDeduction = () => {
    _.each(input, (rawExpression) => {
        if (!_.size(rawExpression))
            return;

        let expression = Parser.parseExp(rawExpression);
        let processed = false;

        if (lastAssumption.valueOf() === expression.valueOf()) {
            processed = true;
            let first = new Implication(expression, new Implication(expression, expression));
            let second = new Implication(expression, new Implication(new Implication(expression, expression), expression));
            let third = new Implication(expression, expression);

            fout.write(first.toString());fout.write('\n');
            fout.write((new Implication(first, new Implication(second, third))).toString());fout.write('\n');
            fout.write((new Implication(second, third)).toString());fout.write('\n');
            fout.write(second.toString());fout.write('\n');
            fout.write(third.toString());fout.write('\n');
        }

        if (!processed) {
            let tryAssumption = _.findIndex(assumptions, (assumption) => assumption.valueOf() === expression.valueOf());
            if (tryAssumption !== -1) {
                processed = true;
                fout.write((new Implication(expression, new Implication(lastAssumption, expression))).toString());fout.write('\n');
                fout.write(expression.toString());fout.write('\n');
                fout.write((new Implication(lastAssumption, expression)).toString());fout.write('\n');
            }
        }

        if (!processed) {
            _.each(parsedAxioms, (axiom, idx) => {
                if (!processed)
                    if (isAxiom(expression, axiom)) {
                        processed = true;
                        fout.write((new Implication(expression, new Implication(lastAssumption, expression))).toString());fout.write('\n');
                        fout.write(expression.toString());fout.write('\n');
                        fout.write((new Implication(lastAssumption, expression)).toString());fout.write('\n');
                    }
            });
        }

        if (!processed) {
            _.each(expressions, (knownExpression) => {
                if (processed)
                    return;

                let tmp = new Implication(knownExpression, expression);
                let tryMP = exprToLine[tmp.valueOf()];
                if (!_.isNil(tryMP)) {
                    processed = true;
                    let first = knownExpression;
                    let second = tmp;

                    let firstExpr = new Implication(lastAssumption, first);
                    let secondExpr = new Implication(lastAssumption, second);
                    let thirdExpr = new Implication(lastAssumption, expression);

                    fout.write((new Implication(firstExpr, new Implication(secondExpr, thirdExpr))).toString());fout.write('\n');
                    fout.write((new Implication(secondExpr, thirdExpr)).toString());fout.write('\n');
                    fout.write(thirdExpr.toString());fout.write('\n');

                }
            })
        }
        expressions.push(expression);
        exprToLine[expression.valueOf()] = line;

        line++;
    });
};

processAssumptions();
performDeduction();