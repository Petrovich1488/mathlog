"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const parser_1 = require("./expression/parser");
const _ = require("lodash");
const expressions_1 = require("./expression/expressions");
const path = require("path");
const utils_1 = require("./expression/utils");
const proof_1 = require("./expression/proof");
const fout = fs_1.createWriteStream(path.join(__dirname, 'input', 'hw3.out'), {
    flags: 'w'
});
let input = _(fs_1.readFileSync(path.join(__dirname, 'input', 'hw3.in'), 'utf8'))
    .split('\n')
    .map((line) => { return _.trim(line); })
    .value();
const buildProof = () => {
    let expression = parser_1.Parser.parseExp(input[0]);
    let tautalogy = utils_1.checkTautalogy(expression);
    if (tautalogy.status) {
        fout.write(`Выражение ложно при ${_.map(tautalogy.ans, ((value, key) => `${key}=${value ? 'И' : 'Л'}`)).join(',')}`);
    }
    else {
        let proofs = [];
        _.each(tautalogy.combos, (combo) => {
            let assumptions = _.map(combo, (val, variable) => {
                return !val ? new expressions_1.Var(variable) : new expressions_1.Not(new expressions_1.Var(variable));
            });
            proofs.push(proof_1.Proof.createProof(expression, assumptions));
        });
        for (let i = 0; i < _.size(tautalogy.variables); i++) {
            let newProofs = [];
            for (let j = 0; j < _.size(proofs); j += 2) {
                newProofs.push(proofs[j].merge(proofs[j + 1]));
            }
            proofs = newProofs;
        }
        proofs[0].print(fout);
    }
};
buildProof();
//# sourceMappingURL=hw3.js.map