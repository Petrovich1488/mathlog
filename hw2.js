"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const parser_1 = require("./expression/parser");
const _ = require("lodash");
const axioms_1 = require("./expression/axioms");
const expressions_1 = require("./expression/expressions");
const path = require("path");
const fout = fs_1.createWriteStream(path.join(__dirname, 'input', 'hw2.out'), {
    flags: 'w'
});
let input = _(fs_1.readFileSync(path.join(__dirname, 'input', 'hw2.in'), 'utf8'))
    .split('\n')
    .map((line) => { return _.trim(line); })
    .value();
let assumptions = [];
let expressions = [];
let exprToLine = {};
let lastAssumption = null;
let line = 1;
const processAssumptions = () => {
    let firstLine = _.first(input);
    input = input.slice(1);
    if (!_.size(firstLine))
        return;
    let list = firstLine.split('|-');
    let rawAssumptions = list[0].split(',');
    _.each(rawAssumptions, (rawAssumption, idx) => {
        if (_.size(rawAssumption)) {
            assumptions[idx] = parser_1.Parser.parseExp(rawAssumption);
            lastAssumption = assumptions[idx];
        }
    });
    fout.write(`${_.initial(rawAssumptions).join(',')} |- (${_.last(rawAssumptions)}) -> (${list[1]})\n`);
};
const performDeduction = () => {
    _.each(input, (rawExpression) => {
        if (!_.size(rawExpression))
            return;
        let expression = parser_1.Parser.parseExp(rawExpression);
        let processed = false;
        if (lastAssumption.valueOf() === expression.valueOf()) {
            processed = true;
            let first = new expressions_1.Implication(expression, new expressions_1.Implication(expression, expression));
            let second = new expressions_1.Implication(expression, new expressions_1.Implication(new expressions_1.Implication(expression, expression), expression));
            let third = new expressions_1.Implication(expression, expression);
            fout.write(first.toString());
            fout.write('\n');
            fout.write((new expressions_1.Implication(first, new expressions_1.Implication(second, third))).toString());
            fout.write('\n');
            fout.write((new expressions_1.Implication(second, third)).toString());
            fout.write('\n');
            fout.write(second.toString());
            fout.write('\n');
            fout.write(third.toString());
            fout.write('\n');
        }
        if (!processed) {
            let tryAssumption = _.findIndex(assumptions, (assumption) => assumption.valueOf() === expression.valueOf());
            if (tryAssumption !== -1) {
                processed = true;
                fout.write((new expressions_1.Implication(expression, new expressions_1.Implication(lastAssumption, expression))).toString());
                fout.write('\n');
                fout.write(expression.toString());
                fout.write('\n');
                fout.write((new expressions_1.Implication(lastAssumption, expression)).toString());
                fout.write('\n');
            }
        }
        if (!processed) {
            _.each(axioms_1.parsedAxioms, (axiom, idx) => {
                if (!processed)
                    if (axioms_1.isAxiom(expression, axiom)) {
                        processed = true;
                        fout.write((new expressions_1.Implication(expression, new expressions_1.Implication(lastAssumption, expression))).toString());
                        fout.write('\n');
                        fout.write(expression.toString());
                        fout.write('\n');
                        fout.write((new expressions_1.Implication(lastAssumption, expression)).toString());
                        fout.write('\n');
                    }
            });
        }
        if (!processed) {
            _.each(expressions, (knownExpression) => {
                if (processed)
                    return;
                let tmp = new expressions_1.Implication(knownExpression, expression);
                let tryMP = exprToLine[tmp.valueOf()];
                if (!_.isNil(tryMP)) {
                    processed = true;
                    let first = knownExpression;
                    let second = tmp;
                    let firstExpr = new expressions_1.Implication(lastAssumption, first);
                    let secondExpr = new expressions_1.Implication(lastAssumption, second);
                    let thirdExpr = new expressions_1.Implication(lastAssumption, expression);
                    fout.write((new expressions_1.Implication(firstExpr, new expressions_1.Implication(secondExpr, thirdExpr))).toString());
                    fout.write('\n');
                    fout.write((new expressions_1.Implication(secondExpr, thirdExpr)).toString());
                    fout.write('\n');
                    fout.write(thirdExpr.toString());
                    fout.write('\n');
                }
            });
        }
        expressions.push(expression);
        exprToLine[expression.valueOf()] = line;
        line++;
    });
};
processAssumptions();
performDeduction();
//# sourceMappingURL=hw2.js.map