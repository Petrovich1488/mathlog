"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const path = require("path");
const _ = require("lodash");
const formalParser_1 = require("./expression/formalParser");
const utils_1 = require("./expression/utils");
const axioms_1 = require("./expression/axioms");
const formalAxioms_1 = require("./expression/formalAxioms");
const expressions_1 = require("./expression/expressions");
Set.prototype.hasValueOf = function (obj) {
    for (let entry of this) {
        if (entry.valueOf() === obj.valueOf()) {
            return true;
        }
    }
    return false;
};
const fout = fs_1.createWriteStream(path.join(__dirname, 'input', 'hw4.out'), {
    flags: 'w'
});
let input = _(fs_1.readFileSync(path.join(__dirname, 'input', 'hw4.in'), 'utf8'))
    .split('\n')
    .map((line) => { return _.trim(line); })
    .value();
const checker = () => {
    let parser = new formalParser_1.FormalParser();
    let rawAssumptions = new Set();
    let assumptionsHash = new Set();
    let free_variables = new Set();
    let expressions = new Set();
    let expression_list = [];
    let prior = [];
    let last_assumption = null;
    let line_number = 0;
    let check = { code: -1, expr: null };
    let [line, main_expression] = input[0].split('|-');
    parser.string = line;
    while (parser.index < _.size(parser.string)) {
        let tmp = parser.parse();
        last_assumption = tmp;
        rawAssumptions.add(tmp);
        assumptionsHash.add(tmp.valueOf());
        if (parser.index < _.size(parser.string) && parser.string[parser.index] == ',') {
            parser.index += 1;
        }
    }
    if (_.size(rawAssumptions) > 0) {
        utils_1.exportFreeVariables(last_assumption, {}, free_variables);
    }
    main_expression = parser.parseExpr(main_expression);
    while (true) {
        line_number++;
        if (line_number % 1000 === 0) {
            console.log(`Обработано ${line_number} строк`);
        }
        let error_string = "[Недоказанное утверждение]";
        line = input[line_number];
        if (!line || _.size(line) === 0) {
            break;
        }
        let expression = parser.parseExpr(line);
        check = { code: -1, expr: null };
        if (axioms_1.isAnyAxiom(expression) || formalAxioms_1.isAnyFormalAxiom(expression)) {
            check = { code: 0, expr: null };
        }
        if (check.code === -1) {
            if (expression instanceof expressions_1.Implication &&
                expression.left instanceof expressions_1.Conjuction &&
                expression.left.right instanceof expressions_1.Any &&
                expression.left.right.val instanceof expressions_1.Implication &&
                utils_1.exportFreeVariables(expression.right, {}, new Set()).has(expression.left.right.var.val) &&
                formalAxioms_1.freeSubtract(expression.right, expression.left.right.val.right, new expressions_1.Var(expression.left.right.var)) &&
                formalAxioms_1.freeSubtract(expression.right, expression.left.left, new expressions_1.Var(expression.left.right.var)) &&
                expression.right.valueOf() === expression.left.right.val.left.valueOf()) {
                check = { code: 0, expr: null };
            }
        }
        if (check.code === -1 && assumptionsHash.has(expression.valueOf()))
            check = { code: 1, expr: null };
        if (check.code === -1) {
            for (var idx = 0; idx < expression_list.length; idx++) {
                let knownExpression = expression_list[_.size(expression_list) - idx - 1];
                let tmp = new expressions_1.Implication(knownExpression, expression);
                let tryMP = expressions.has(tmp.valueOf());
                if (tryMP) {
                    check = { code: 2, expr: knownExpression };
                    break;
                }
            }
        }
        if (check.code === -1) {
            if (expression instanceof expressions_1.Implication && expression.right instanceof expressions_1.Any) {
                let tmp = new expressions_1.Implication(expression.left, expression.right.val);
                if (expressions.has(tmp.valueOf())) {
                    let freeVars = utils_1.exportFreeVariables(expression.left);
                    if (!freeVars.has(expression.right.var.val)) {
                        if (!free_variables.has(expression.right.var.val))
                            check = { code: 3, expr: null };
                        else
                            error_string = `[Невозможно переделать доказательство. Применение правил с кватором всеобщности, используещее свободную переменную ${expression.right.var.toString()} из предположений.]`;
                    }
                    else {
                        error_string = `[Ошибка применения правил вывода с квантором всеобщности. Переменная ${expression.right.var.toString()} входит свободно.]`;
                    }
                }
            }
        }
        if (check.code === -1) {
            if (expression instanceof expressions_1.Implication && expression.left instanceof expressions_1.Exists) {
                let tmp = new expressions_1.Implication(expression.left.val, expression.right);
                if (expressions.has(tmp.valueOf())) {
                    if (!utils_1.exportFreeVariables(expression.right).has(expression.left.var.val)) {
                        if (!free_variables.has(expression.left.var.val))
                            check = { code: 4, expr: null };
                        else
                            error_string = `[Невозможно переделать доказательство. Применение правил с квантором существования, используещее свободную переменную ${expression.left.var.toString()} из предположений.]`;
                    }
                    else {
                        error_string = `[Ошибка применения правил вывода с квантором существования. Переменная ${expression.left.var.toString()} входит свободно.]`;
                    }
                }
            }
        }
        if (check.code === -1) {
            console.log('Вывод некорректен');
            fout.write(`Вывод некорректен, начиная с формулы № ${line_number} : ${error_string}\n`);
            fout.write(line);
            break;
        }
        else {
            expressions.add(expression.valueOf());
            expression_list.push(expression);
            prior.push(check);
        }
    }
    if (check.code !== -1) {
        console.log('Вывод корректен');
        if (_.size(rawAssumptions) > 0) {
            let newAssumptions = [...rawAssumptions]
                .filter((assumption) => assumption.valueOf() !== last_assumption.valueOf())
                .map((assumption) => assumption.toString());
            fout.write(`${newAssumptions.join(',')}|-(${last_assumption.toString()})->(${main_expression.toString()})\n`);
            let anyProof = fs_1.readFileSync(path.join(__dirname, 'hw4proofs', '@rule.proof'), 'utf8').split('\n');
            let existsProof = fs_1.readFileSync(path.join(__dirname, 'hw4proofs', 'questionrule.proof'), 'utf8').split('\n');
            expression_list.forEach((expression, index) => {
                if (prior[index].code === 0) { //axiom
                    fout.write(new expressions_1.Implication(expression, new expressions_1.Implication(last_assumption, expression)).toString());
                    fout.write('\n');
                    fout.write(`${expression.toString()}\n`);
                    fout.write(`${new expressions_1.Implication(last_assumption, expression).toString()}\n`);
                }
                else if (prior[index].code === 1) {
                    //assumption
                    if (expression.valueOf() === last_assumption.valueOf()) {
                        let tmp1 = new expressions_1.Implication(last_assumption, new expressions_1.Implication(last_assumption, last_assumption));
                        fout.write(`${tmp1.toString()}\n`);
                        let tmp2 = new expressions_1.Implication(last_assumption, new expressions_1.Implication(new expressions_1.Implication(last_assumption, last_assumption), last_assumption));
                        let tmp3 = new expressions_1.Implication(last_assumption, last_assumption);
                        let tmp4 = new expressions_1.Implication(tmp1, new expressions_1.Implication(tmp2, tmp3));
                        let tmp5 = new expressions_1.Implication(tmp2, tmp3);
                        fout.write(`${tmp4.toString()}\n`);
                        fout.write(`${tmp5.toString()}\n`);
                        fout.write(`${tmp2.toString()}\n`);
                        fout.write(`${tmp3.toString()}\n`);
                    }
                    else {
                        let tmp1 = new expressions_1.Implication(expression, new expressions_1.Implication(last_assumption, expression));
                        let tmp2 = new expressions_1.Implication(last_assumption, expression);
                        fout.write(`${tmp1.toString()}\n`);
                        fout.write(`${expression.toString()}\n`);
                        fout.write(`${tmp2.toString()}\n`);
                    }
                }
                else if (prior[index].code === 2) {
                    // MP
                    let tmp = new expressions_1.Implication(last_assumption, new expressions_1.Implication(prior[index].expr, expression));
                    let tmp1 = new expressions_1.Implication(last_assumption, prior[index].expr);
                    let tmp2 = new expressions_1.Implication(last_assumption, expression);
                    let tmp4 = new expressions_1.Implication(tmp1, new expressions_1.Implication(tmp, tmp2));
                    let tmp5 = new expressions_1.Implication(tmp, tmp2);
                    fout.write(`${tmp4.toString()}\n`);
                    fout.write(`${tmp5.toString()}\n`);
                    fout.write(`${tmp2.toString()}\n`);
                }
                else if (prior[index].code === 3) {
                    // ANY
                    let variables = {
                        "A": last_assumption,
                        "B": expression.left,
                        "C": expression.right.val,
                        "x": expression.right.var
                    };
                    anyProof.forEach((proofLine) => {
                        fout.write(`${parser.createExpression(proofLine, variables).toString()}\n`);
                    });
                }
                else if (prior[index].code === 4) {
                    // EXISTS
                    let variables = {
                        "A": last_assumption,
                        "B": expression.left.val,
                        "C": expression.right,
                        "x": expression.left.var
                    };
                    existsProof.forEach((proofLine) => {
                        fout.write(`${parser.createExpression(proofLine, variables).toString()}\n`);
                    });
                }
            });
        }
        else {
            fout.write(`|-${main_expression.toString()}\n`);
            expression_list.forEach((expression) => fout.write(`${expression.toString()}\n`));
        }
    }
};
checker();
//# sourceMappingURL=hw4.js.map