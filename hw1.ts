import { readFileSync, createWriteStream } from "fs";
import {Parser} from "./expression/parser";
import * as _ from 'lodash';
import {isAxiom, parsedAxioms} from "./expression/axioms";
import {Expression, Implication} from "./expression/expressions";
import * as path from "path";
import * as os from "os";

const fout = createWriteStream(path.join(__dirname, 'input', 'hw1.out'), {
    flags: 'w'
});

let input = _(readFileSync(path.join(__dirname, 'input', 'hw1.in'), 'utf8'))
            .split('\n')
            .map((line) => { return _.trim(line); })
            .value();
let assumptions:Array<Expression> = [];
let expressions:Array<Expression> = [];
let exprToLine = {};
let line = 1;

const parseAssumptions = () => {
    let list = input[0].split('|-');
    if (_.size(list) === 2) {
        let rawAssumptions = list[0].split(',');
        _.each(rawAssumptions, (rawAssumption, idx) => {
            if (_.size(rawAssumption))
                assumptions[idx] = Parser.parseExp(rawAssumption);
        });
    }
    fout.write(input[0] + '\n');
    input = input.slice(1);
};

const annotate = () => {
    _.each(input, (rawExpression) => {
        if (!_.size(rawExpression))
            return;

        let expression = Parser.parseExp(rawExpression);

        let annotationInfo:any = {state: 0, num: -1};

        let tryAssumption = _.findIndex(assumptions, (assumption) => assumption.valueOf() === expression.valueOf());
        if (tryAssumption !== -1) {
            annotationInfo = {state: 1, num: tryAssumption + 1};
        }

        if (annotationInfo.state === 0)
            _.each(parsedAxioms, (axiom, idx) => {
                if (annotationInfo.state === 0)
                    if (isAxiom(expression, axiom))
                        annotationInfo = {state: 2, num: idx + 1};
            });

        if (annotationInfo.state === 0) {
            _.each(expressions, (knownExpression) => {
                if (annotationInfo.state !== 0)
                    return;

                let tmp = new Implication(knownExpression, expression);
                let tryMP = exprToLine[tmp.valueOf()];
                if (!_.isNil(tryMP))
                    annotationInfo = {state: 3, num: [exprToLine[knownExpression.valueOf()], exprToLine[tmp.valueOf()]]};
            })
        }

        fout.write(`( ${line} ) ${rawExpression} `);
        switch (annotationInfo.state) {
            case 0:
                fout.write('(Не доказано)');
                break;
            case 1:
                fout.write(`(Предположение ${annotationInfo.num})`);
                break;
            case 2:
                fout.write(`(Сх. акс. ${annotationInfo.num})`);
                break;
            case 3:
                let [first,second] = annotationInfo.num;
                fout.write(`(M.P. ${first}, ${second})`);
                break;
            default: break;
        }

        fout.write('\n');

        if (annotationInfo.state !== 0) {
            expressions.push(expression);
            exprToLine[expression.valueOf()] = line;
        }

        line++;
    });
};

parseAssumptions();
annotate();