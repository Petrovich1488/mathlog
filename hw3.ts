import { readFileSync, createWriteStream } from "fs";
import {Parser} from "./expression/parser";
import * as _ from 'lodash';
import {isAxiom, parsedAxioms} from "./expression/axioms";
import {Expression, Implication, Not, Var} from "./expression/expressions";
import * as path from "path";
import * as os from "os";
import {checkTautalogy} from "./expression/utils";
import {Proof} from "./expression/proof";

const fout = createWriteStream(path.join(__dirname, 'input', 'hw3.out'), {
    flags: 'w'
});

let input = _(readFileSync(path.join(__dirname, 'input', 'hw3.in'), 'utf8'))
    .split('\n')
    .map((line) => { return _.trim(line); })
    .value();

const buildProof = () => {
    let expression = Parser.parseExp(input[0]);
    let tautalogy = checkTautalogy(expression);

    if (tautalogy.status) {
        fout.write(`Выражение ложно при ${_.map(tautalogy.ans, ((value, key) => `${key}=${value?'И':'Л'}`)).join(',')}`);
    } else {
        let proofs:Array<Proof> = [];
        _.each(tautalogy.combos, (combo) => {
            let assumptions = _.map(combo, (val, variable) : Expression => {
                return !val ? new Var(variable) : new Not(new Var(variable));
            });

            proofs.push(Proof.createProof(expression, assumptions));
        });

        for(let i = 0; i < _.size(tautalogy.variables); i++) {
            let newProofs = [];
            for(let j = 0; j < _.size(proofs); j+=2) {
                newProofs.push(proofs[j].merge(proofs[j + 1]));
            }
            proofs = newProofs;
        }

        proofs[0].print(fout);
    }
};

buildProof();
